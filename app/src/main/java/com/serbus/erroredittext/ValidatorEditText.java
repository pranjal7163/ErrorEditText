package com.serbus.erroredittext;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by webwerk on 14/7/16.
 */
public class ValidatorEditText extends RelativeLayout {
	private EditText       errorEditText;
	private RelativeLayout errorLayout;
	private int errorDrawable,successDrawable;
	TextView errorText;
	LayoutInflater mInflater;

	public ValidatorEditText( Context context ) {
		super( context );
		mInflater = LayoutInflater.from( context );
		init();
	}

	public ValidatorEditText( Context context, AttributeSet attrs ) {
		super( context, attrs );
		mInflater = LayoutInflater.from(context);
		init();
	}

	public ValidatorEditText( Context context, AttributeSet attrs, int defStyleAttr ) {
		super( context, attrs, defStyleAttr );
		mInflater = LayoutInflater.from(context);
		init();
	}

	public void init(){
		View v = mInflater.inflate(R.layout.error_edittext, this, true);
		errorEditText = (EditText) v.findViewById(R.id.error_edit);
		errorLayout = (RelativeLayout) v.findViewById( R.id.errorLayout );
		errorText = (TextView) v.findViewById( R.id.error_text );
	}
	public void setTextWatcher( final int successDrawable,int errorDrawable){
		this.errorDrawable = errorDrawable;
		this.successDrawable = successDrawable;
		errorEditText.addTextChangedListener( new TextWatcher() {
			@Override
			public void beforeTextChanged( CharSequence s, int start, int count, int after ) {

			}

			@Override
			public void onTextChanged( CharSequence s, int start, int before, int count ) {
				String editString = s.toString();
				if(s.length()!=0){
				if (!isValidPassword( editString )) {
					setError( );
				}else{
					errorEditText.setCompoundDrawablesWithIntrinsicBounds( 0,0,successDrawable,0 );
					errorLayout.setVisibility( View.GONE );
				}}else{
					errorEditText.setCompoundDrawablesWithIntrinsicBounds( 0,0,0,0 );
					errorLayout.setVisibility( View.GONE );
				}
			}

			@Override
			public void afterTextChanged( Editable s ) {

			}
		} );
	}

	@Override
	public void setBackground( Drawable background ) {
		super.setBackground( background );
		errorEditText.setBackground( background );
	}

	private void setError(){
		errorLayout.setVisibility( View.VISIBLE );
		errorEditText.setCompoundDrawablesWithIntrinsicBounds(0,0, errorDrawable,0 );
		errorLayout.setBackgroundResource( android.R.color.holo_red_light );
		errorText.setText( "Include atlest one upper case, one lower case, one digit and one speical character" );
		errorText.setTextColor( getResources().getColor( android.R.color.white ) );

	}

	public boolean isValidPassword(final String password) {

		Pattern pattern;
		Matcher matcher;

		final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=*!])(?=\\S+$).{4,}$";

		pattern = Pattern.compile(PASSWORD_PATTERN);
		matcher = pattern.matcher(password);

		return matcher.matches();

	}
}
