package com.serbus.erroredittext;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
		private ValidatorEditText validatorEditText;
	@Override
	protected void onCreate( Bundle savedInstanceState ) {
		super.onCreate( savedInstanceState );
		setContentView( R.layout.activity_main );
		validatorEditText = (ValidatorEditText)findViewById( R.id.validatorEditText );
		validatorEditText.setTextWatcher(R.drawable.ok,R.drawable.highimportance);
		validatorEditText.setBackgroundResource( R.drawable.custom_edittext );
	}
}
